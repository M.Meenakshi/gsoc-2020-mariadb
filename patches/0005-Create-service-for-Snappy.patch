From 5a9344c2aac5998be1e53ced738e7ed8e1dbc7e8 Mon Sep 17 00:00:00 2001
From: Kartik Soneji <kartiksoneji@rocketmail.com>
Date: Fri, 28 Aug 2020 04:01:24 +0530
Subject: [PATCH 05/15] Create service for Snappy.

---
 cmake/plugin.cmake                            |  2 +
 include/compression/snappy-c.h                | 63 +++++++++++++++++++
 include/mysql/plugin_audit.h.pp               | 17 +++++
 include/mysql/plugin_auth.h.pp                | 17 +++++
 include/mysql/plugin_data_type.h.pp           | 17 +++++
 include/mysql/plugin_encryption.h.pp          | 17 +++++
 include/mysql/plugin_ftparser.h.pp            | 17 +++++
 include/mysql/plugin_function.h.pp            | 17 +++++
 include/mysql/plugin_password_validation.h.pp | 17 +++++
 include/mysql/services.h                      |  1 +
 include/service_versions.h                    |  1 +
 libmysqld/CMakeLists.txt                      |  1 +
 libservices/CMakeLists.txt                    |  1 +
 libservices/compression_service_snappy.c      | 14 +++++
 sql/CMakeLists.txt                            |  1 +
 sql/compression/compression_libs.cc           | 14 +++--
 sql/compression/compression_libs.h            | 15 +++--
 sql/compression/snappy.cc                     | 54 ++++++++++++++++
 sql/mysqld.cc                                 |  1 +
 sql/sql_plugin.cc                             |  3 +-
 sql/sql_plugin_services.ic                    | 10 +--
 21 files changed, 283 insertions(+), 17 deletions(-)
 create mode 100644 include/compression/snappy-c.h
 create mode 100644 libservices/compression_service_snappy.c
 create mode 100644 sql/compression/snappy.cc

diff --git a/cmake/plugin.cmake b/cmake/plugin.cmake
index cedeed6b..e2fc5195 100644
--- a/cmake/plugin.cmake
+++ b/cmake/plugin.cmake
@@ -290,6 +290,7 @@ MACRO(MYSQL_ADD_PLUGIN)
       -DHAVE_BZIP2
       -DHAVE_LZMA
       -DHAVE_LZO
+      -DHAVE_SNAPPY
     )
 
     GET_PROPERTY(LINK_LIBRARIES DIRECTORY PROPERTY LINK_LIBRARIES)
@@ -297,6 +298,7 @@ MACRO(MYSQL_ADD_PLUGIN)
       bz2
       lzma
       lzo2 liblzo2.a
+      snappy
     )
     SET_PROPERTY(DIRECTORY PROPERTY LINK_LIBRARIES "${LINK_LIBRARIES}")
 
diff --git a/include/compression/snappy-c.h b/include/compression/snappy-c.h
new file mode 100644
index 00000000..79d6b71e
--- /dev/null
+++ b/include/compression/snappy-c.h
@@ -0,0 +1,63 @@
+/**
+  @file include/compression/snappy-c.h
+  This service provides dynamic access to Snappy as a C header.
+*/
+
+#ifndef SERVICE_SNAPPY_C_INCLUDED
+#ifdef __cplusplus
+extern "C" {
+#endif
+
+#ifndef MYSQL_ABI_CHECK
+#include <stddef.h>
+#include <stdbool.h>
+#endif
+
+extern bool COMPRESSION_LOADED_SNAPPY;
+
+typedef enum{
+    SNAPPY_OK                 = 0,
+    SNAPPY_INVALID_INPUT      = 1,
+    SNAPPY_BUFFER_TOO_SMALL   = 2
+} snappy_status;
+
+#define DEFINE_snappy_max_compressed_length(NAME) size_t NAME(  \
+    size_t source_length                                        \
+)
+
+#define DEFINE_snappy_compress(NAME) snappy_status NAME(    \
+    const char *input,                                      \
+    size_t input_length,                                    \
+    char *compressed,                                       \
+    size_t *compressed_length                               \
+)
+
+#define DEFINE_snappy_uncompress(NAME) snappy_status NAME(  \
+    const char *compressed,                                 \
+    size_t compressed_length,                               \
+    char *uncompressed,                                     \
+    size_t *uncompressed_length                             \
+)
+
+typedef DEFINE_snappy_max_compressed_length ((*PTR_snappy_max_compressed_length));
+typedef DEFINE_snappy_compress              ((*PTR_snappy_compress));
+typedef DEFINE_snappy_uncompress            ((*PTR_snappy_uncompress));
+
+struct compression_service_snappy_st{
+    PTR_snappy_max_compressed_length snappy_max_compressed_length_ptr;
+    PTR_snappy_compress              snappy_compress_ptr;
+    PTR_snappy_uncompress            snappy_uncompress_ptr;
+};
+
+extern struct compression_service_snappy_st *compression_service_snappy;
+
+#define snappy_max_compressed_length(...) compression_service_snappy->snappy_max_compressed_length_ptr (__VA_ARGS__)
+#define snappy_compress(...)              compression_service_snappy->snappy_compress_ptr              (__VA_ARGS__)
+#define snappy_uncompress(...)            compression_service_snappy->snappy_uncompress_ptr            (__VA_ARGS__)
+
+#ifdef __cplusplus
+}
+#endif
+
+#define SERVICE_SNAPPY_C_INCLUDED
+#endif
diff --git a/include/mysql/plugin_audit.h.pp b/include/mysql/plugin_audit.h.pp
index 8e1edc88..c4bc2d7d 100644
--- a/include/mysql/plugin_audit.h.pp
+++ b/include/mysql/plugin_audit.h.pp
@@ -547,6 +547,23 @@ struct compression_service_lzo_st{
 };
 extern struct compression_service_lzo_st *compression_service_lzo;
 }
+extern "C" {
+extern bool COMPRESSION_LOADED_SNAPPY;
+typedef enum{
+    SNAPPY_OK = 0,
+    SNAPPY_INVALID_INPUT = 1,
+    SNAPPY_BUFFER_TOO_SMALL = 2
+} snappy_status;
+typedef size_t (*PTR_snappy_max_compressed_length)( size_t source_length );
+typedef snappy_status (*PTR_snappy_compress)( const char *input, size_t input_length, char *compressed, size_t *compressed_length );
+typedef snappy_status (*PTR_snappy_uncompress)( const char *compressed, size_t compressed_length, char *uncompressed, size_t *uncompressed_length );
+struct compression_service_snappy_st{
+    PTR_snappy_max_compressed_length snappy_max_compressed_length_ptr;
+    PTR_snappy_compress snappy_compress_ptr;
+    PTR_snappy_uncompress snappy_uncompress_ptr;
+};
+extern struct compression_service_snappy_st *compression_service_snappy;
+}
 }
 struct st_mysql_xid {
   long formatID;
diff --git a/include/mysql/plugin_auth.h.pp b/include/mysql/plugin_auth.h.pp
index 5500015d..9d77c8a9 100644
--- a/include/mysql/plugin_auth.h.pp
+++ b/include/mysql/plugin_auth.h.pp
@@ -547,6 +547,23 @@ struct compression_service_lzo_st{
 };
 extern struct compression_service_lzo_st *compression_service_lzo;
 }
+extern "C" {
+extern bool COMPRESSION_LOADED_SNAPPY;
+typedef enum{
+    SNAPPY_OK = 0,
+    SNAPPY_INVALID_INPUT = 1,
+    SNAPPY_BUFFER_TOO_SMALL = 2
+} snappy_status;
+typedef size_t (*PTR_snappy_max_compressed_length)( size_t source_length );
+typedef snappy_status (*PTR_snappy_compress)( const char *input, size_t input_length, char *compressed, size_t *compressed_length );
+typedef snappy_status (*PTR_snappy_uncompress)( const char *compressed, size_t compressed_length, char *uncompressed, size_t *uncompressed_length );
+struct compression_service_snappy_st{
+    PTR_snappy_max_compressed_length snappy_max_compressed_length_ptr;
+    PTR_snappy_compress snappy_compress_ptr;
+    PTR_snappy_uncompress snappy_uncompress_ptr;
+};
+extern struct compression_service_snappy_st *compression_service_snappy;
+}
 }
 struct st_mysql_xid {
   long formatID;
diff --git a/include/mysql/plugin_data_type.h.pp b/include/mysql/plugin_data_type.h.pp
index 6a0e2f1f..76f0d9a3 100644
--- a/include/mysql/plugin_data_type.h.pp
+++ b/include/mysql/plugin_data_type.h.pp
@@ -547,6 +547,23 @@ struct compression_service_lzo_st{
 };
 extern struct compression_service_lzo_st *compression_service_lzo;
 }
+extern "C" {
+extern bool COMPRESSION_LOADED_SNAPPY;
+typedef enum{
+    SNAPPY_OK = 0,
+    SNAPPY_INVALID_INPUT = 1,
+    SNAPPY_BUFFER_TOO_SMALL = 2
+} snappy_status;
+typedef size_t (*PTR_snappy_max_compressed_length)( size_t source_length );
+typedef snappy_status (*PTR_snappy_compress)( const char *input, size_t input_length, char *compressed, size_t *compressed_length );
+typedef snappy_status (*PTR_snappy_uncompress)( const char *compressed, size_t compressed_length, char *uncompressed, size_t *uncompressed_length );
+struct compression_service_snappy_st{
+    PTR_snappy_max_compressed_length snappy_max_compressed_length_ptr;
+    PTR_snappy_compress snappy_compress_ptr;
+    PTR_snappy_uncompress snappy_uncompress_ptr;
+};
+extern struct compression_service_snappy_st *compression_service_snappy;
+}
 }
 struct st_mysql_xid {
   long formatID;
diff --git a/include/mysql/plugin_encryption.h.pp b/include/mysql/plugin_encryption.h.pp
index dccd2f8c..17480c91 100644
--- a/include/mysql/plugin_encryption.h.pp
+++ b/include/mysql/plugin_encryption.h.pp
@@ -547,6 +547,23 @@ struct compression_service_lzo_st{
 };
 extern struct compression_service_lzo_st *compression_service_lzo;
 }
+extern "C" {
+extern bool COMPRESSION_LOADED_SNAPPY;
+typedef enum{
+    SNAPPY_OK = 0,
+    SNAPPY_INVALID_INPUT = 1,
+    SNAPPY_BUFFER_TOO_SMALL = 2
+} snappy_status;
+typedef size_t (*PTR_snappy_max_compressed_length)( size_t source_length );
+typedef snappy_status (*PTR_snappy_compress)( const char *input, size_t input_length, char *compressed, size_t *compressed_length );
+typedef snappy_status (*PTR_snappy_uncompress)( const char *compressed, size_t compressed_length, char *uncompressed, size_t *uncompressed_length );
+struct compression_service_snappy_st{
+    PTR_snappy_max_compressed_length snappy_max_compressed_length_ptr;
+    PTR_snappy_compress snappy_compress_ptr;
+    PTR_snappy_uncompress snappy_uncompress_ptr;
+};
+extern struct compression_service_snappy_st *compression_service_snappy;
+}
 }
 struct st_mysql_xid {
   long formatID;
diff --git a/include/mysql/plugin_ftparser.h.pp b/include/mysql/plugin_ftparser.h.pp
index 1ad3792a..48d29f47 100644
--- a/include/mysql/plugin_ftparser.h.pp
+++ b/include/mysql/plugin_ftparser.h.pp
@@ -547,6 +547,23 @@ struct compression_service_lzo_st{
 };
 extern struct compression_service_lzo_st *compression_service_lzo;
 }
+extern "C" {
+extern bool COMPRESSION_LOADED_SNAPPY;
+typedef enum{
+    SNAPPY_OK = 0,
+    SNAPPY_INVALID_INPUT = 1,
+    SNAPPY_BUFFER_TOO_SMALL = 2
+} snappy_status;
+typedef size_t (*PTR_snappy_max_compressed_length)( size_t source_length );
+typedef snappy_status (*PTR_snappy_compress)( const char *input, size_t input_length, char *compressed, size_t *compressed_length );
+typedef snappy_status (*PTR_snappy_uncompress)( const char *compressed, size_t compressed_length, char *uncompressed, size_t *uncompressed_length );
+struct compression_service_snappy_st{
+    PTR_snappy_max_compressed_length snappy_max_compressed_length_ptr;
+    PTR_snappy_compress snappy_compress_ptr;
+    PTR_snappy_uncompress snappy_uncompress_ptr;
+};
+extern struct compression_service_snappy_st *compression_service_snappy;
+}
 }
 struct st_mysql_xid {
   long formatID;
diff --git a/include/mysql/plugin_function.h.pp b/include/mysql/plugin_function.h.pp
index e92f1c22..b70befaf 100644
--- a/include/mysql/plugin_function.h.pp
+++ b/include/mysql/plugin_function.h.pp
@@ -547,6 +547,23 @@ struct compression_service_lzo_st{
 };
 extern struct compression_service_lzo_st *compression_service_lzo;
 }
+extern "C" {
+extern bool COMPRESSION_LOADED_SNAPPY;
+typedef enum{
+    SNAPPY_OK = 0,
+    SNAPPY_INVALID_INPUT = 1,
+    SNAPPY_BUFFER_TOO_SMALL = 2
+} snappy_status;
+typedef size_t (*PTR_snappy_max_compressed_length)( size_t source_length );
+typedef snappy_status (*PTR_snappy_compress)( const char *input, size_t input_length, char *compressed, size_t *compressed_length );
+typedef snappy_status (*PTR_snappy_uncompress)( const char *compressed, size_t compressed_length, char *uncompressed, size_t *uncompressed_length );
+struct compression_service_snappy_st{
+    PTR_snappy_max_compressed_length snappy_max_compressed_length_ptr;
+    PTR_snappy_compress snappy_compress_ptr;
+    PTR_snappy_uncompress snappy_uncompress_ptr;
+};
+extern struct compression_service_snappy_st *compression_service_snappy;
+}
 }
 struct st_mysql_xid {
   long formatID;
diff --git a/include/mysql/plugin_password_validation.h.pp b/include/mysql/plugin_password_validation.h.pp
index 70ea08c2..69001306 100644
--- a/include/mysql/plugin_password_validation.h.pp
+++ b/include/mysql/plugin_password_validation.h.pp
@@ -547,6 +547,23 @@ struct compression_service_lzo_st{
 };
 extern struct compression_service_lzo_st *compression_service_lzo;
 }
+extern "C" {
+extern bool COMPRESSION_LOADED_SNAPPY;
+typedef enum{
+    SNAPPY_OK = 0,
+    SNAPPY_INVALID_INPUT = 1,
+    SNAPPY_BUFFER_TOO_SMALL = 2
+} snappy_status;
+typedef size_t (*PTR_snappy_max_compressed_length)( size_t source_length );
+typedef snappy_status (*PTR_snappy_compress)( const char *input, size_t input_length, char *compressed, size_t *compressed_length );
+typedef snappy_status (*PTR_snappy_uncompress)( const char *compressed, size_t compressed_length, char *uncompressed, size_t *uncompressed_length );
+struct compression_service_snappy_st{
+    PTR_snappy_max_compressed_length snappy_max_compressed_length_ptr;
+    PTR_snappy_compress snappy_compress_ptr;
+    PTR_snappy_uncompress snappy_uncompress_ptr;
+};
+extern struct compression_service_snappy_st *compression_service_snappy;
+}
 }
 struct st_mysql_xid {
   long formatID;
diff --git a/include/mysql/services.h b/include/mysql/services.h
index 5ca081f0..e39f87bc 100644
--- a/include/mysql/services.h
+++ b/include/mysql/services.h
@@ -47,6 +47,7 @@ extern "C" {
 #include <compression/bzlib.h>
 #include <compression/lzma.h>
 #include <compression/lzo/lzo1x.h>
+#include <compression/snappy-c.h>
 
 #ifdef __cplusplus
 }
diff --git a/include/service_versions.h b/include/service_versions.h
index d8d579df..cb3cf1ce 100644
--- a/include/service_versions.h
+++ b/include/service_versions.h
@@ -48,3 +48,4 @@
 #define VERSION_compression_bzip2       0x0100
 #define VERSION_compression_lzma        0x0100
 #define VERSION_compression_lzo         0x0100
+#define VERSION_compression_snappy      0x0100
diff --git a/libmysqld/CMakeLists.txt b/libmysqld/CMakeLists.txt
index 0c182f73..fbc91b1d 100644
--- a/libmysqld/CMakeLists.txt
+++ b/libmysqld/CMakeLists.txt
@@ -142,6 +142,7 @@ SET(SQL_EMBEDDED_SOURCES emb_qcache.cc libmysqld.c lib_sql.cc
            ../libservices/compression_service_bzip2.c
            ../libservices/compression_service_lzma.c
            ../libservices/compression_service_lzma.c
+           ../libservices/compression_service_snappy.c
 )
 
 
diff --git a/libservices/CMakeLists.txt b/libservices/CMakeLists.txt
index dbe9bc64..b3703fc3 100644
--- a/libservices/CMakeLists.txt
+++ b/libservices/CMakeLists.txt
@@ -42,6 +42,7 @@ SET(MYSQLSERVICES_SOURCES
   compression_service_bzip2.c
   compression_service_lzma.c
   compression_service_lzo.c
+  compression_service_snappy.c
   )
 
 ADD_CONVENIENCE_LIBRARY(mysqlservices ${MYSQLSERVICES_SOURCES})
diff --git a/libservices/compression_service_snappy.c b/libservices/compression_service_snappy.c
new file mode 100644
index 00000000..22983f3d
--- /dev/null
+++ b/libservices/compression_service_snappy.c
@@ -0,0 +1,14 @@
+/* Copyright (C) 2017 MariaDB Corporation
+   This program is free software; you can redistribute it and/or modify
+   it under the terms of the GNU General Public License as published by
+   the Free Software Foundation; version 2 of the License.
+   This program is distributed in the hope that it will be useful,
+   but WITHOUT ANY WARRANTY; without even the implied warranty of
+   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
+   GNU General Public License for more details.
+   You should have received a copy of the GNU General Public License
+   along with this program; if not, write to the Free Software
+   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1335  USA */
+
+#include <service_versions.h>
+SERVICE_VERSION compression_service_snappy = (void*) VERSION_compression_snappy;
diff --git a/sql/CMakeLists.txt b/sql/CMakeLists.txt
index 9ec89bb1..ed3c31eb 100644
--- a/sql/CMakeLists.txt
+++ b/sql/CMakeLists.txt
@@ -169,6 +169,7 @@ SET (SQL_SOURCE
                compression/bzip2.cc  ../libservices/compression_service_bzip2.c
                compression/lzma.cc   ../libservices/compression_service_lzma.c
                compression/lzo.cc    ../libservices/compression_service_lzo.c
+               compression/snappy.cc ../libservices/compression_service_snappy.c
 )
   
 IF ((CMAKE_SYSTEM_NAME MATCHES "Linux" OR
diff --git a/sql/compression/compression_libs.cc b/sql/compression/compression_libs.cc
index 7a65a7e9..c3b72ed1 100644
--- a/sql/compression/compression_libs.cc
+++ b/sql/compression/compression_libs.cc
@@ -3,9 +3,10 @@
 #include <mysqld.h>
 
 void init_compression(
-    struct compression_service_bzip2_st *bzip2_handler,
-    struct compression_service_lzma_st  *lzma_handler,
-    struct compression_service_lzo_st   *lzo_handler
+    struct compression_service_bzip2_st  *bzip2_handler,
+    struct compression_service_lzma_st   *lzma_handler,
+    struct compression_service_lzo_st    *lzo_handler,
+    struct compression_service_snappy_st *snappy_handler
 ){
     if(enabled_compression_libraries & COMPRESSION_ALL)
         enabled_compression_libraries = (COMPRESSION_ALL) - 1;
@@ -25,7 +26,8 @@ void init_compression(
 
     // TODO: Handle different versions of the library (liblzma.so.1, liblzma.so.2, etc)
 
-    init_bzip2 (bzip2_handler,  (enabled_compression_libraries & COMPRESSION_BZIP2));
-    init_lzma  (lzma_handler,   (enabled_compression_libraries & COMPRESSION_LZMA));
-    init_lzo   (lzo_handler,    (enabled_compression_libraries & COMPRESSION_LZO));
+    init_bzip2  (bzip2_handler,  (enabled_compression_libraries & COMPRESSION_BZIP2));
+    init_lzma   (lzma_handler,   (enabled_compression_libraries & COMPRESSION_LZMA));
+    init_lzo    (lzo_handler,    (enabled_compression_libraries & COMPRESSION_LZO));
+    init_snappy (snappy_handler, (enabled_compression_libraries & COMPRESSION_SNAPPY));
 }
diff --git a/sql/compression/compression_libs.h b/sql/compression/compression_libs.h
index 62f4cfd2..48086724 100644
--- a/sql/compression/compression_libs.h
+++ b/sql/compression/compression_libs.h
@@ -1,6 +1,7 @@
 #include <compression/bzlib.h>
 #include <compression/lzma.h>
 #include <compression/lzo/lzo1x.h>
+#include <compression/snappy-c.h>
 
 #define COMPRESSION_BZIP2   1 << 0
 #define COMPRESSION_LZ4     1 << 1
@@ -13,11 +14,13 @@
 
 
 void init_compression(
-    struct compression_service_bzip2_st *,
-    struct compression_service_lzma_st  *,
-    struct compression_service_lzo_st   *
+    struct compression_service_bzip2_st  *,
+    struct compression_service_lzma_st   *,
+    struct compression_service_lzo_st    *,
+    struct compression_service_snappy_st *
 );
 
-void init_bzip2 (struct compression_service_bzip2_st  *, bool);
-void init_lzma  (struct compression_service_lzma_st   *, bool);
-void init_lzo   (struct compression_service_lzo_st    *, bool);
+void init_bzip2  (struct compression_service_bzip2_st  *, bool);
+void init_lzma   (struct compression_service_lzma_st   *, bool);
+void init_lzo    (struct compression_service_lzo_st    *, bool);
+void init_snappy (struct compression_service_snappy_st *, bool);
diff --git a/sql/compression/snappy.cc b/sql/compression/snappy.cc
new file mode 100644
index 00000000..b1490447
--- /dev/null
+++ b/sql/compression/snappy.cc
@@ -0,0 +1,54 @@
+#include "compression_libs.h"
+#include <compression/snappy.h>
+#include <dlfcn.h>
+
+bool COMPRESSION_LOADED_SNAPPY = false;
+
+
+DEFINE_snappy_max_compressed_length(DUMMY_snappy_max_compressed_length){
+    return 0;
+}
+
+DEFINE_snappy_compress(DUMMY_snappy_compress){
+    *compressed_length = 0;
+    return SNAPPY_INVALID_INPUT;
+}
+
+DEFINE_snappy_uncompress(DUMMY_snappy_uncompress){
+    return SNAPPY_INVALID_INPUT;
+}
+
+
+void init_snappy(struct compression_service_snappy_st *handler, bool load_library){
+    //point struct to right place for static plugins
+    compression_service_snappy = handler;
+
+    compression_service_snappy->snappy_max_compressed_length_ptr = DUMMY_snappy_max_compressed_length;
+    compression_service_snappy->snappy_compress_ptr              = DUMMY_snappy_compress;
+    compression_service_snappy->snappy_uncompress_ptr            = DUMMY_snappy_uncompress;
+
+    if(!load_library)
+        return;
+
+    //Load Snappy library dynamically
+    void *library_handle = dlopen("libsnappy.so", RTLD_LAZY | RTLD_GLOBAL);
+    if(!library_handle || dlerror())
+        return;
+
+    void *snappy_max_compressed_length_ptr = dlsym(library_handle, "snappy_max_compressed_length");
+    void *snappy_compress_ptr              = dlsym(library_handle, "snappy_compress");
+    void *snappy_uncompress_ptr            = dlsym(library_handle, "snappy_uncompress");
+
+    if(
+        !snappy_max_compressed_length_ptr ||
+        !snappy_compress_ptr              ||
+        !snappy_uncompress_ptr
+    )
+        return;
+    
+    compression_service_snappy->snappy_max_compressed_length_ptr = (PTR_snappy_max_compressed_length) snappy_max_compressed_length_ptr;
+    compression_service_snappy->snappy_compress_ptr              = (PTR_snappy_compress)              snappy_compress_ptr;
+    compression_service_snappy->snappy_uncompress_ptr            = (PTR_snappy_uncompress)            snappy_uncompress_ptr;
+
+    COMPRESSION_LOADED_SNAPPY = true;
+}
diff --git a/sql/mysqld.cc b/sql/mysqld.cc
index e36e8fbe..5aaf9bfc 100644
--- a/sql/mysqld.cc
+++ b/sql/mysqld.cc
@@ -7321,6 +7321,7 @@ SHOW_VAR status_vars[]= {
   {"Compression_loaded_bzip2", (char*) &COMPRESSION_LOADED_BZIP2,  SHOW_BOOL},
   {"Compression_loaded_lzma",  (char*) &COMPRESSION_LOADED_LZMA,   SHOW_BOOL},
   {"Compression_loaded_lzo",   (char*) &COMPRESSION_LOADED_LZO,    SHOW_BOOL},
+  {"Compression_loaded_snappy",(char*) &COMPRESSION_LOADED_SNAPPY, SHOW_BOOL},
   {"Connections",              (char*) &global_thread_id,         SHOW_LONG_NOFLUSH},
   {"Connection_errors_accept", (char*) &connection_errors_accept, SHOW_LONG},
   {"Connection_errors_internal", (char*) &connection_errors_internal, SHOW_LONG},
diff --git a/sql/sql_plugin.cc b/sql/sql_plugin.cc
index 7988a2d6..73be5b4f 100644
--- a/sql/sql_plugin.cc
+++ b/sql/sql_plugin.cc
@@ -1640,7 +1640,8 @@ int plugin_init(int *argc, char **argv, int flags)
   init_compression(
     &compression_handler_bzip2,
     &compression_handler_lzma,
-    &compression_handler_lzo
+    &compression_handler_lzo,
+    &compression_handler_snappy
   );
 
   /* prepare encryption_keys service */
diff --git a/sql/sql_plugin_services.ic b/sql/sql_plugin_services.ic
index 268ec271..de53e3be 100644
--- a/sql/sql_plugin_services.ic
+++ b/sql/sql_plugin_services.ic
@@ -229,9 +229,10 @@ static struct thd_mdl_service_st thd_mdl_handler=
 };
 
 // updated in init_compression()
-static struct compression_service_bzip2_st  compression_handler_bzip2 = {};
-static struct compression_service_lzma_st   compression_handler_lzma  = {};
-static struct compression_service_lzo_st    compression_handler_lzo   = {};
+static struct compression_service_bzip2_st  compression_handler_bzip2  = {};
+static struct compression_service_lzma_st   compression_handler_lzma   = {};
+static struct compression_service_lzo_st    compression_handler_lzo    = {};
+static struct compression_service_snappy_st compression_handler_snappy = {};
 
 static struct st_service_ref list_of_services[]=
 {
@@ -261,5 +262,6 @@ static struct st_service_ref list_of_services[]=
   { "thd_mdl_service",             VERSION_thd_mdl,             &thd_mdl_handler },
   { "compression_service_bzip2",   VERSION_compression_bzip2,   &compression_handler_bzip2 },
   { "compression_service_lzma",    VERSION_compression_lzma,    &compression_handler_lzma },
-  { "compression_service_lzo",     VERSION_compression_lzo,     &compression_handler_lzo }
+  { "compression_service_lzo",     VERSION_compression_lzo,     &compression_handler_lzo },
+  { "compression_service_snappy",  VERSION_compression_snappy,  &compression_handler_snappy }
 };
-- 
2.26.2.windows.1

